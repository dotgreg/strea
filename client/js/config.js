const conf = {
		filesPath: `http://142.4.213.86:2002`,
		LogPath: `http://142.4.213.86:2002/test.txt`,
		histPath: `http://142.4.213.86:2002/history.txt`,
		videoHlsPath: `http://142.4.213.86:2002/test.m3u8`,
		videoPlayerHtmlPath: `.`,

		appUrl: `http://142.4.213.86:2001`,
		downloadUrl: `http://142.4.213.86:2003`, // should be pointing on folderpaht
		folderPath: `/home/sigis/streaming/fichiers`,

		labels: {
				// movies: '',
				// series: ''
				movies: 'movies',
				series: 'series'
		}
}
