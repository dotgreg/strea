
/*********************************************
 * 
 * DETAILS VIEW
 * 
 */

const StructItemComp = (p) => {
		const {item, onItemClick, blacklist} = {...p}
		let title = cleanStringFromBlacklist(item.title, blacklist)
		// let title = item.title
		let isFile = item.type === 'file'
		const getAbsPath = (path) => {
				let newp = path.replace(conf.folderPath, conf.downloadUrl)
				return newp
		}
		return (
				!item.title.endsWith('.nfo') && !item.title.endsWith('.srt') &&
						<ul>
						
						<div 
				className={`struct-item-title ${isFile ? 'is-file' : ''}`}> 
						{ item.type === 'file' && <span className='file-icon'> {`🎞️`} </span>}
						<span onClick={e => {
								isFile && onItemClick(item)
						}}>{title}  </span>
						{/* <span 
								onClick={e => {
								console.log('dl ',getAbsPath(item.path))
								window.open(getAbsPath(item.path),'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,')
								}}> dl </span> */}
				{ item.type === 'file' && <a href={getAbsPath(item.path)} target="_blank"> ⬇ </a>}
				
				</div>
						{
								item.children && item.children.map(child => 
										!child.title.endsWith('.nfo') && !child.title.endsWith('.srt') &&
												<li>
												<StructItemComp item={child} blacklist={blacklist} onItemClick={onItemClick} />
												</li>
								)
						} 
				</ul>
		)
}

const DetailsItemComp = (p) => {
		const {item, onItemClick} = {...p}
		
		let blacklist = getChildrenWordsBlacklist(item.children)
		// let blacklist = []
		// React.useEffect(() => {
		// 	// console.log(blacklistWords);
		// },[])

		return (
						<div className='detail'>
						<div className='close' onClick={e => {
								p.onclose()
						}}>X</div>
						<div className="detail-wrapper">
						<div className="left">
						
						<div className="img-wrapper">
						{/* <div className="img" 
								style={{backgroundImage: `url('${item.imageUrl}')`}}>
								</div> */}
						<img  src={item.imageUrl} />
						</div>
						</div>
						<div className="right">
						<div className="item-description">
						<h1>{item.title}</h1>
						<div className="overview">
						{item.overview}
				</div>
						</div>
						<div className="structure">
						{
								item.children.map(child => 
												<StructItemComp item={child} blacklist={blacklist} onItemClick={onItemClick} />
								)
						}
				</div>
						</div>
						</div>
						</div>
		)
}

