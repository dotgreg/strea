
 const SubtitleComp = (p) => {

  	const [subtitleDecalSec, setSubtitleDecalSec] = React.useState(0);
  	const [srtContent, setSrtContent] = React.useState('');

	React.useEffect(() => {
		convertSrtAndSend(srtContent, subtitleDecalSec)
	}, [p.jumpHour, p.jumpMin])

	const convertSrtAndSend = (nSrt, nDecalSec) => {
		if (nSrt === '') return
		const fullDecal = [-p.jumpHour, -p.jumpMin, nDecalSec]
		const nVtt = srt2webvtt(nSrt, fullDecal)
		// const base64Content = `data:@file/octet-stream;base64,` + btoa(nVtt);
		const nBlob  = new Blob([nVtt], {type : 'text/vtt'});
		p.onSubtitleChange({content: nVtt, blob: nBlob, fullDecal: JSON.stringify(fullDecal)})
	}
	  
	const subtitleSearchUrl = `https://www.google.com/search?q=subtitle ${p.title}`
	return (
		<div>
			<br/>
			Subtitles -
			<a className="loglink" href={subtitleSearchUrl} target="_blank">
				search
			</a> 
			<br/>
			<input
				type="file"
				// value={selectedFile}
				onChange={(e) => {
					const selectedFile = e.target.files[0]
					readFile(selectedFile, (nSrt) => {
						setSrtContent(nSrt)
						convertSrtAndSend(nSrt, subtitleDecalSec)
					})
				}}
			/>
			decal: 
			<input type="number" min='-60' max='60' value={subtitleDecalSec} onChange={e => {
				const ndecalsec = e.target.value
				setSubtitleDecalSec(ndecalsec);
				convertSrtAndSend(srtContent, ndecalsec)
			}} />
		</div>
	)
}

