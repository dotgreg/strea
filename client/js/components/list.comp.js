/***********************************************
 * 
 * LIST
 * 
 */

const FileComp = (p) => {
		const {item, onclick} = {...p}
		const cleanTitle = cleanArtTitle(cleanString(item.title))

		const [imageUrl, setImageUrl] = useLocalStorage(`imageUrl-${cleanTitle}`, null)
		const [overview, setOverview] = useLocalStorage(`overview-${cleanTitle}`, null)
		const [randomColorBg, setRandomColorBg] = useLocalStorage(`randomcolor-${cleanTitle}`, null)

		
		if (!imageUrl && !randomColorBg) {
				const urlApi1 = `https://api.themoviedb.org/3/search/${p.mediaType === 'series' ? 'tv' : 'movie'}?api_key=17b3095ce44e1eddece1045b84735657&query=${cleanTitle}&page=1`
				get(urlApi1,data => {
						// get first result image
						console.log(`themoviedb answer for ${cleanTitle}`, data)
						if (!data.results || !data.results[0] || !data.results[0].backdrop_path) {
								setRandomColorBg(`${rand(0,255)},${rand(0,255)},${rand(0,255)}`)
						} else {
								const objRes = data.results[0]
								// const image = `https://image.tmdb.org/t/p/w500/${objRes.backdrop_path}`
								const image = `https://image.tmdb.org/t/p/w500/${objRes.poster_path}`
								setImageUrl(image)
								setOverview(objRes.overview)
						}
				})
		} else {
				// console.log('there is one oooooo', imageUrl)
		}
		return (
				<div 
						className="file-overview" 
						style={{backgroundImage: `url('${imageUrl}')`, backgroundColor: `rgba(${randomColorBg}, .3)`}}
						onClick={e => {
								onclick({
										rawApi:item,
										children: item.children,
										path:item.path,
										title:item.title,
										overview,
										imageUrl,
										cleanTitle
								})
						}}
				>
						<div 
								className={`starred`}
								onClick={e => {
										p.onStarClick(item.title);
										e.stopPropagation()
								}}
						>
								<i class={`${p.isStarred ? 'fas' : 'far'} fa-star`} aria-hidden="true"></i>
						</div>
						<div className={`title ${imageUrl ? `hover-only` : ''}`}>{cleanTitle} </div>
				</div>
		)
}

const ListComp = (p) => {
		const [items, setItems] = React.useState([])
		const [searchedItem, setSearchedItem] = React.useState('')
		const [starredItems, setStarredItems] = useLocalStorage('starred-items',[])

		const [mediaType, setMediaType] = React.useState('series')
		const {forceUpdate, forceUpdateVal, shouldShow} = useForceUpdate();

		const url = `${conf.appUrl}/api?path=${conf.folderPath}/${mediaType}/&action=list`
		React.useEffect(()=>{
				setItems([])
				get(url,data => {
						let newItems = data.children
						if (mediaType === 'movies') {
								newItems = processMoviesData(data.children)
						} else {

						}
						console.log(newItems);
						setItems(reorgItems(newItems))
				})
		},[mediaType])

		React.useEffect(()=>{
				setItems(reorgItems(items))
				console.log(1212);
		},[forceUpdateVal])

		const reorgItems = (itemsToReorg) => {
				const starredArr = []
				const nonStarredArr = []
				for (let i = 0; i < itemsToReorg.length; i++) {
						if (starredItems.includes(itemsToReorg[i].title)) starredArr.push(itemsToReorg[i])
						else nonStarredArr.push(itemsToReorg[i])
				}
				return [...starredArr, ...nonStarredArr]
		}
		
		const onStarClick = title =>{
				let newArr = starredItems
				if(starredItems.includes(title)) newArr = starredItems.filter(e => e !== title)
				else newArr.push(title)
				setStarredItems(newArr)
				forceUpdate()
		}

		return (
				<div>
						<div className="types-tabs"> 
								<div className={`type ${mediaType === 'movies' ? 'active' : ''}`} onClick={e=>{setMediaType('movies')}}> {conf.labels.movies} </div> 
								<div className={`type ${mediaType === 'series' ? 'active' : ''}`} onClick={e=>{setMediaType('series')}}> {conf.labels.series} </div> 
						</div>
						<div className="search-item"> 
								<input type="text" value={searchedItem} onChange={e => setSearchedItem(e.target.value)} />
						</div>
						<div className="list-overview">
								{	shouldShow &&
									items.map(item => 
											(searchedItem === '' || item.title.toLowerCase().includes(searchedItem.toLowerCase())) &&
													<FileComp 
															item={item} 
															isStarred={starredItems.includes(item.title)}
															onStarClick={ onStarClick}
															mediaType={mediaType} 
															onclick={p.onItemClick} 
													/>
									)
								}
						</div>
				</div>
		)
}

