
/*********************************************
 * 
 * VIDEO POPUP
 * 
 */

 const VideoPlayerComp = (p) => {
	const {playlistPath, item} = {...p}
	const [fullscreen, setFullscreen] = React.useState(false)
	const [quality, setQuality] = useLocalStorage('video-quality',30)
	const [audio, setAudio] = useLocalStorage('video-audio',0)
	const [volume, setVolume] = useLocalStorage('video-volume',1.5)
	const [audioStereo, setAudioStereo] = useLocalStorage('video-audio-stereo',1)
	const [audioQuality, setAudioQuality] = useLocalStorage('video-audio-quality',96)
	const [speed, setSpeed] = useLocalStorage('video-speed',6)
	const [cacheTime, setCacheTime] = useLocalStorage('player-cache-time',120)
	const [titleItem, setTitleItem] = useLocalStorage('video-title-item','')
	const [streamStats, setStreamStats] = React.useState('')
	// not used right now
	const [size, setSize] = useLocalStorage('video-size',1200)
	const [streamId, setStreamId] = useLocalStorage('video-stream-id','test')
	const [timeHour, setTimeHour] = React.useState(0)
	const [timeMin, setTimeMin] = React.useState(0)
	const [timeMinInt, setTimeMinInt] = React.useState(0)
	const [timeSec, setTimeSec] = React.useState(0)
	const [reloadVideoPlayer, setReloadVideoPlayer] = React.useState(false)
	const [iframeSrc, setIframeSrc] = React.useState()

	// const [subTitleBase64, setSubTitleBase64] = React.useState('')

	// React.useEffect(() => {
	// 	setTitleItem(item.title)
	// }, [item.title])

	// const streamId = 1
	const startVideoApiUrlArr = [
		conf.appUrl + '/api',
		`?path=${encodeURIComponent(item.path)}&`,
		`&quality=${quality}`,
		`&audio=${audio}`,
		`&volume=${volume}`,
		`&audioquality=${audioQuality}`,
		`&stereo=${audioStereo}`,
		`&size=${size}`,
		`&hour=${timeHour}`,
		`&speed=${speed}`,
		`&min=${timeMin}`,
		`&sec=${timeSec}`,
		`&action=stream`,
		``,
	]
	const startVideoApiUrl = startVideoApiUrlArr.join('')

	const startServerStream = () => {
		console.log(`startServerStream `, startVideoApiUrl, 'encoded path: ', encodeURI(item.path));
		get(startVideoApiUrl)
		startVideo()
	}

	React.useEffect(() => {
		setInterval(() => {
			get(`${conf.appUrl}/api?action=streamstats`, data => {
				console.log(data.stats)
				setStreamStats(data.stats)
			})
		}, 4000)
	}, [])

	const startVideo = () => {
		// 1 send req to api
		console.log(`startVideo`);
		setReloadVideoPlayer(true);
		setTimeout(() => {
			setReloadVideoPlayer(false)
			const videoPlaylistPath = p.playlistPath
			// const codepenUrlVideojs = `https://cdpn.io/dotgreg/debug/wvoLLKK/xnMabmwovnmr?path=https://open-public-343536.websocial.cc/stream/test.m3u8`
			// const codepenUrlHls = `https://cdpn.io/dotgreg/debug/rNjXLjb/VJMxxEnKEPXM?path=https://open-public-343536.websocial.cc/stream/test.m3u8`
			const videoPlayerHls = `${conf.videoPlayerHtmlPath}/hls.html?cachetime=${cacheTime}&path=${videoPlaylistPath}`
			console.log('Reloading iframe : ',videoPlayerHls);
			setIframeSrc(`${videoPlayerHls}`)
		}, 100)
	}

	

	return (
		<div className={`video-component ${fullscreen ? 'fullscreen' : ''}`}>
			<div className='video-wrapper'>
				{/* {!reloadVideoPlayer && <video controls id="video"></video>} */}
				{!reloadVideoPlayer && <iframe id="video" allowfullscreen="true" src={iframeSrc}></iframe>}

				<div className='control-wrapper'>


					<div className='control-part text-config'>
						<div className="title-item">
							{item.title}
						</div>
						<a className="loglink" href={conf.LogPath} target="_blank">
							logs
						</a>
						<a className="loglink" href={conf.filesPath} target="_blank">
							files
						</a>
						<a className="loglink" href={conf.histPath} target="_blank">
							hist
						</a>
					</div>


					<div className='control-part stream-config'>
						<div className="control quality input" >
							<input type="number" min='1' step="1" max='50' value={quality} onChange={e => {setQuality(e.target.value);}} />
							<br/>quality
						</div>
						<div className="control speed input" >
							<input type="number" min='0'max='9' value={speed} onChange={e => {setSpeed(e.target.value);}} />
							<br/>enc speed
						</div>
						<div className="control audio input small" >
							<input type="number" min='0' max='5' value={audio} onChange={e => {setAudio(e.target.value);}} />
							<br/>audio_track
						</div>
						<div className="control time input small" >
							<input type="number" min='0' max='500' value={timeMinInt} onChange={e => {
								setTimeHour(Math.floor(e.target.value/60));
								setTimeMin(e.target.value % 60 );
								setTimeMinInt(e.target.value);
							}} />
							<br/>time
							{/* :<input type="number" min='100' max='2000' value={time} onChange={e => {setTime(e.target.value);}} /> */}
						</div>
						<div className="control button input" >
							<button
								onClick={() => {
									startServerStream()
									setTitleItem(item.title)
									

									let int = setInterval(() => {
										get(`${conf.appUrl}/api?action=countfiles`, data => {
											console.log(data.count)
											if (!data) clearInterval(int)
											if (data && data.count > 4) {
												startVideo()
												clearInterval(int)
											}
										})
									}, 1000)

								}}
							>encode </button>
						</div> 
					</div> 

					<br></br>
					<div className='control-part stream-config'>

						<div className="control audio input small" >
							<input type="number" min='0' max='1000' value={audioQuality} onChange={e => {setAudioQuality(e.target.value);}} />
							<br/>audio_qual
						</div>

						<div className="control audio input small" >
							<input type="number" min='0' max='1' value={audioStereo} onChange={e => {setAudioStereo(e.target.value);}} />
							<br/>stereo
						</div>

						<div className="control audio input small" >
							<input type="number" min='0' step="0.5" max='5' value={volume} onChange={e => {setVolume(e.target.value);}} />
							<br/>volume
						</div>
						
						<div>{streamStats}</div>
					</div>
					<br></br>


					<div className='control-part video-config'>
						<div className="control cachetime input small" >
							<input type="number" value={cacheTime} onChange={e => {setCacheTime(e.target.value);}} />
							<br/>cache_time
						</div>
						<button
							onClick={() => {startVideo()}}
						>
							<i class={`fa fa-redo`} aria-hidden="true"></i>
						</button>

						<button className="control fullscreen" onClick={e => {
							setFullscreen(!fullscreen)
							p.onFullscreenToggle(!fullscreen)
						}}> 
							<i class={`fa fa-${ fullscreen ? `compress` : `expand`}`} aria-hidden="true"></i>
						</button>

						<button className="control close" onClick={e => {console.log('fsdfs');p.onClose()}}>
							<i class={`fa fa-times`} aria-hidden="true"></i>
						</button>
					</div> 

					<div>
					<SubtitleComp 
						title={item.title}
						jumpHour={timeHour}
						jumpMin={timeMin}
						onSubtitleChange={(vttObj) => {
							console.log('[MAIN] new vtt', vttObj)
							// send it to hls with window.parent which is shared
							window.subtitleObj = vttObj
						}}
					/>
					</div>



				</div> 
			</div>
		</div>
	)
}


