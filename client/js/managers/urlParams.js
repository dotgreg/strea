
const getUrlParam = (key) => {
    const queryString = window.location.search;
    const urlParamsSearch = new URLSearchParams(queryString);
    return urlParamsSearch.get(key) || "";
};

const isDemoMode = () => getUrlParam("demo") !== "";
const isLogMode = () => getUrlParam("log") !== "";

console.log('hello world');
