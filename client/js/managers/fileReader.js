const readFile = (file, onRead) => {
    // var file = e.target.files[0];
    if (!file) {
      return;
    }
    var reader = new FileReader();
    reader.onload = function(e) {
      const srtContent = e.target.result;
      onRead(srtContent)
    };
    reader.readAsText(file);
}