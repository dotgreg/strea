// const convert() {
//     var input = document.getElementById("srt");
//     var output = document.getElementById("webvtt");
//     var srt = input.value;
//     var webvtt = srt2webvtt(srt);
//     output.innerHTML = "<textarea rows=20 cols=80>"
//                        + webvtt +
//                        "</textarea>";
//   }

  const srt2webvtt = (data, decal) => {
    if(!decal) decal = [0,0,0]
    // remove dos newlines
    var srt = data.replace(/\r+/g, '');
    // trim white space start and end
    srt = srt.replace(/^\s+|\s+$/g, '');

    // get cues
    var cuelist = srt.split('\n\n');
    var result = "";

    if (cuelist.length > 0) {
      result += "WEBVTT\n\n";
      for (var i = 0; i < cuelist.length; i=i+1) {
        result += convertSrtCue(cuelist[i], decal);
      }
    }

    return result;
  }

  const convertSrtCue = (caption, dec) => {
    // remove all html tags for security reasons
    //srt = srt.replace(/<[a-zA-Z\/][^>]*>/g, '');

    var cue = "";
    var s = caption.split(/\n/);

    // concatenate muilt-line string separated in array into one
    while (s.length > 3) {
        for (var i = 3; i < s.length; i++) {
            s[2] += "\n" + s[i]
        }
        s.splice(3, s.length - 3);
    }

    var line = 0;

    // detect identifier
    if (!s[0].match(/\d+:\d+:\d+/) && s[1].match(/\d+:\d+:\d+/)) {
      cue += s[0].match(/\w+/) + "\n";
      line += 1;
    }

    // get time strings
    if (s[line].match(/\d+:\d+:\d+/)) {
      // convert time string
      var m = s[1].match(/(\d+):(\d+):(\d+)(?:,(\d+))?\s*--?>\s*(\d+):(\d+):(\d+)(?:,(\d+))?/);

      const d = (val, dec) => {
        const nVal = parseInt(val) + parseInt(dec)
        // if ()
        return nVal
      }
      let h1 = d(m[1],dec[0])
      let m1 = d(m[2],dec[1])
      let s1 = d(m[3],dec[2])
      let ms1 = d(m[4],0)
      if (s1 > 60) { let nMin = Math.round(s1 / 60); s1 = s1 % 60; m1 += nMin}
      if (s1 < 0) { let nMin = Math.floor(s1 / 60); s1 = 60 + s1; m1 += nMin}
      if (m1 > 60) { let nHour = Math.round(m1 / 60); m1 = m1 % 60; h1 += nHour}
      if (m1 < 0) { let nHour = Math.floor(m1 / 60); m1 = 60 + m1; h1 += nHour}
      
      let h2 = d(m[5],dec[0])
      let m2 = d(m[6],dec[1])
      let s2 = d(m[7],dec[2])
      let ms2 = d(m[8],0)
      if (s2 > 60) { let nMin = Math.round(s2 / 60); s2 = s2 % 60; m2 += nMin}
      if (s2 < 0) { let nMin = Math.floor(s2 / 60); s2 = 60 + s2; m2 += nMin}
      if (m2 > 60) { let nHour = Math.round(m2 / 60); m2 = m2 % 60; h2 += nHour}
      if (m2 < 0) { let nHour = Math.floor(m2 / 60); m2 = 60 + m2; h2 += nHour}

      // if (ms1 > 100) ms1 = Math.round(ms1 / 10)
      // if (ms2 > 100) ms2 = Math.round(ms2 / 10)
      if (ms1 < 100) ms1 = ms1 * 10
      if (ms2 < 100) ms2 = ms2 * 10

      const f = (valInt) => {
        let str = `${valInt}`
        if (str.length === 1) str = '0'+str
        return str
      }


      if (m) {
        cue += f(h1)+":"+f(m1)+":"+f(s1)+"."+f(ms1)+" --> "
              +f(h2)+":"+f(m2)+":"+f(s2)+"."+f(ms2)+"\n";
        line += 1;
      } else {
        // Unrecognized timestring
        return "";
      }
    } else {
      // file format error or comment lines
      return "";
    }

    // get cue text
    if (s[line]) {
      cue += s[line] + "\n\n";
    }

    return cue;
  }