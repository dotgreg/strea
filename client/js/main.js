const App = (p) => {
	const [activeItem, setActiveItem] = React.useState()
	const [itemPlaying, setItemPlaying] = React.useState()
	const [isFullscreen, setIsFullscreen] = React.useState()

	let currView = 'list-view'
	if (activeItem) currView = 'detail-view'
	if (isFullscreen) currView = 'fullscreen-video-view'
	// if (itemPlaying) currView = 'detail-view'

	React.useEffect(() => {
		if (isDemoMode()) {
			setItemPlaying({
				title: 'test',
				type: 'file',
				imageUrl: '',
				path: 'test',
				children: [],
			})
		}
	}, [])

	return (
		<div role="dialog" className={`${currView} app-container ${isIpad() && 'ipad'}`}>
			{
				activeItem && 
				<DetailsItemComp 
					item={activeItem} 
					onItemClick={item => { 
						setItemPlaying()
						setTimeout(() => {
							console.log('video opened to', item);
							setItemPlaying(item)
						}, 10)
					}}
					onclose={() => { setActiveItem() }}
				/>
			} 
			<ListComp 
				onItemClick={item => {
				console.log('(app) item clicked',item)
				setActiveItem(item)
			}} />
			
			{ itemPlaying && <VideoPlayerComp 
				item={itemPlaying} 
				playlistPath={conf.videoHlsPath} 
				playlistPath={conf.videoHlsPath} 
				onClose={e => {setItemPlaying(); setIsFullscreen(false);}} 
				onFullscreenToggle={isF => {setIsFullscreen(isF)}}
			/>}

		</div>
	)
}

ReactDOM.render(<App />, document.querySelector(".container"));

























































/*******************************************************************************************
 * @1 FUNCTIONS
 *******************************************************************************************/

const createNewMovieObj = (child) => {
	console.log(child.title)
	let res = {
		...child,
		children:[
			child
		]
	}
	console.log(res);
	return res
}
const isMovieFile = title => {
	const noBlacklist = !title.includes('sample')
	const endsWith = title.endsWith('mp4') || title.endsWith('m4v') || title.endsWith('avi') || title.endsWith('mkv')
	return noBlacklist && endsWith
}
const processMoviesData = (children) => {
	let newArr = []

	const scanIter = (children, resArr) => {
		children.map(child => {
			if (child.type === 'file' && isMovieFile(child.title)) resArr.push(createNewMovieObj(child))
			else if (child.type === 'folder' && child.children) {
				scanIter(child.children, resArr)
			}
		})
	}

	scanIter(children, newArr)
	console.log({newArr});
	return newArr
}

const cleanStringFromBlacklist = (string, blacklistWords) => {
	let nstring = cleanString(string)
	for (let i = 0; i < blacklistWords.length; i++) {
		nstring = nstring.replace(blacklistWords[i], '');
	}
	return nstring
}

const getChildrenWordsBlacklist = (rootChildren) => {
	let wordCounts = {}
	let blacklist = []
	iterOnName(rootChildren, wordCounts)
	let treshHold = Math.max(wordCounts.__highercount/5, 1)
	for (const key in wordCounts) {
		if (Object.hasOwnProperty.call(wordCounts, key)) {
			const word = wordCounts[key];
			if (wordCounts[key] > treshHold) blacklist.push(key)
		}
	}
	// console.log({wordCounts});
	return blacklist
}
const iterOnName = (rootChildren, wordCounts) => {
	for (let i = 0; i < rootChildren.length; i++) {
		const item = rootChildren[i];
		let title = cleanString(item.title)
		let titleArr = title.split(' ')
		wordCounts.__highercount= 0
		for (let j = 0; j < titleArr.length; j++) {
			let word = titleArr[j];
			if (!wordCounts[word]) wordCounts[word] = 1
			else wordCounts[word] += 1

			if (wordCounts[word] > wordCounts.__highercount) wordCounts.__highercount = wordCounts[word]
		}
		if (item.children) iterOnName(item.children, wordCounts)
	}
}

const get = (url, cb, cbErr) => {
	fetch(url)
	.then((resp) => resp.json())
	.then(data => {
		cb(data)
	})
	.catch(err => {
		if (cbErr) cbErr(err)
	});
}

const cleanString = title => {
	return title.toLowerCase().replace(/[|&;$%@"'`{}\]\[<>()+,]/g, "").replace(/[.-]/g, " ").replace(/[_]/g, " ").trim();
}
const blacklistInLowercase = ['m720p','imax','internal','d4r10','x254','criterion','vo','mpeg4','divx','ddp5.1','dsnp', 'screener','dvdscr','dvdrip','webrip','hddvd','yify','www','mp4','mkv','avi','vf2','dvdrip','complete','h264','torrent','subtitle','subtitles','jpg','com','xvid','brrip','truefrench','bdrip','avi','10bit','hdrip','vff','french','en','fr','hdlight','multi','compete','complete','first','season','series','720p','web','dl','x264','1080p','h265','joy','vostfr','integrale','bluray','x265','hevc','qc','s01' ,'s02','s03','s04','s05','s06','s07','s08','s09','x265','lund','saison','amzn','heteam','vf','hdtv','tracks' ,'arte','meech','ac3','aac2','ultrabitch','msub','mw']
const cleanArtTitle = title => {
	let titleArr = title.split(' ')
	titleArr = titleArr.slice(0, 5)
	// console.log(titleArr);
	let newArrTitle = []
	for (let i = 0; i < titleArr.length; i++) {
		const word = titleArr[i];
		if (blacklistInLowercase.includes(word.toLowerCase())) {}
		else if (word.length === 1 && isNumeric(word)) {}
		else if (word.length === 4 && isNumeric(word)) {}
		else newArrTitle.push(word)
	}
	return newArrTitle.join(' ').trim()
}

function useForceUpdate(){
    const [value, setValue] = React.useState(0); // integer state
    const [shouldShow, setShouldShow] = React.useState(true); // integer state

    return {forceUpdate:() => {
		setValue(value => value + 1)
		setShouldShow(false)
		setTimeout(() => {setShouldShow(true)})
	}, forceUpdateVal:value, shouldShow}; // update the state to force render
}


function isNumeric(str) {
	if (typeof str != "string") return false // we only process strings!  
	return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
		   !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
  }


function useLocalStorage(key, initialValue) {
	const [storedValue, setStoredValue] = React.useState(() => {
	  try {
		const item = window.localStorage.getItem(key);
		return item ? JSON.parse(item) : initialValue;
	  } catch (error) {
		console.log(error);
		return initialValue;
	  }
	});
	const setValue = value => {
	  try {
		const valueToStore =
		  value instanceof Function ? value(storedValue) : value;
		setStoredValue(valueToStore);
		window.localStorage.setItem(key, JSON.stringify(valueToStore));
	  } catch (error) {
		console.log(error);
	  }
	};
	return [storedValue, setValue];
  }


  function rand(min, max) {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min;
  }
  
  const isA = (device) => {
	return deviceType() === device
  }
  
  function isIpad() {
	if (/iPad/.test(navigator.platform)) {
	  return true;
	} else {
	  return navigator.maxTouchPoints &&
		navigator.maxTouchPoints > 2 &&
		/MacIntel/.test(navigator.platform);
	}
  }
    
  const deviceType = () => {
	  let deviceWidth = window.innerWidth
	  let res = 'desktop'
	  if (deviceWidth < 1000) res = 'tablet'
	  if (deviceWidth < 500) res = 'mobile'
	  return res
  }