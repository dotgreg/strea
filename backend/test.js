var exec = require('child_process').exec;
function execute(command, callback){
    exec(command, function(error, stdout, stderr){ callback(stdout); });
};
const pathstream = '/home/greg/open_public/stream'
const cmdSize = `du -sh ${pathstream}`
const cmdNb = `ls ${pathstream} | wc -l`
execute(cmdSize, (answerSize) => {
    execute(cmdNb, (answerNb) => {
        let arr = answerSize.split('M')
        let raw = arr[0]
        let resSizeInt = parseInt(raw)
        console.log(resSizeInt/2)

        let resNbInt = parseInt(answerNb) -2 
        console.log(resNbInt)

        const sizePFile = resSizeInt / resNbInt
        const sizePerSec = sizePFile / 10
        const sizePerSecInK = Math.round(sizePerSec * 1000)
        const sizePerHour = Math.round(sizePerSec * 60 * 60)

        const resStr = `${sizePerHour}M/h ${sizePerSecInK}k/s`
        console.log(resStr)
    });
});