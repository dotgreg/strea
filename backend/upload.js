const conf = require('./config.js')
const fileUpload = require('express-fileupload');

exports.initUploadLogic = (appExpress) => {
    appExpress.use(fileUpload());

    appExpress.post('/api/upload', function(req, res) {
        console.log('[UPLOAD] processing new request', req.files.file)
      
        if (!req.files || Object.keys(req.files).length === 0) {
          return res.status(400).send('No files were uploaded.');
        }
      
        const fileUploaded = req.files.file;
        // console.log(fileUploaded.name);
        const uploadPath = conf.params.pathSubtitles + '/' + fileUploaded.name;
      
        fileUploaded.mv(uploadPath, function(err) {
          if (err)
            return res.status(500).send(err);

            // p.onUploadDone()
      
          res.send('[UPLOAD] File uploaded to ' + uploadPath);
        });
      });
}