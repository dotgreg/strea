var fs = require('fs');
var path = require('path');
var walk = function(dir, done) {
  var results = [];
  fs.readdir(dir, function(err, list) {
    if (err) return done(err);
    var i = 0;
    (function next() {
      var file = list[i++];
      if (!file) return done(null, results);
      file = path.resolve(dir, file);
      fs.stat(file, function(err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function(err, res) {
            results = results.concat(res);
            next();
          });
        } else {
          results.push(file);
          next();
        }
      });
    })();
  });
};

// export const scandir = (dir) => {
//     const fullFolderPath = require('path').join(__dirname, `../${dir}`)
//     if (!fileExists(fullFolderPath)) return
//     var fileStats = fs.lstatSync(fullFolderPath)
//     const resultFolder = {
//         title: path.basename(fileStats),
//     };
//     if (folderStats.isDirectory()) {
//         resultFolder.files = []
//         fs.readdirSync(fullFolderPath).map((child) => {
//             let fullChildPath = fullFolderPath + '/' + child
//             try {
//                 let childStats = fs.lstatSync(fullChildPath)
//                 if (childStats.isDirectory() && dirDefaultBlacklist.indexOf(path.basename(child)) === -1) {
//                     // if (fullChildPath.indexOf('.tiro') !== -1) console.log('1212', fullChildPath)
//                     let relativeChildFolder = fullChildPath.replace(backConfig.dataFolder, '')
//                     let childFolder:iFolder = {
//                         hasChildren: false,
//                         path: relativeChildFolder,
//                         title: path.basename(relativeChildFolder),
//                         key: relativeChildFolder
//                     }

//                     // WITHOUT READDIR 0.026s
//                     // WITH READDIR fullChildPath 0.026s kiff kiff
//                     // WITH READDIR fullChildPath + loop 0.026s kiff kiff
//                     // WITH READDIR fullChildPath + loop + lstatSync + isDir 0.300s /10 perfs
//                     // WITH READDIR fullChildPath + loop + checkDir from name 0.030s x10 times faster
//                     // LAST + CONSOLE LOG EACH ITE = 4s = PERFS /100!!!
                    
//                     const subchildren:string[] = fs.readdirSync(fullChildPath)
//                     for (let i = 0; i < subchildren.length; i++) {
//                         const child2 = subchildren[i];
//                         let fullChild2Path = fullChildPath + '/' + child2

//                         // take in account .folder like .tiro and .trash
//                         let child2temp = child2[0] === '.' ? child2.substr(1) : child2
//                         const hasExtension = child2temp.split('.').length > 1

//                         if (dirDefaultBlacklist.indexOf(path.basename(child2)) === -1 && !hasExtension) {
//                             childFolder.hasChildren = true
//                             break;
//                         }

//                         // 10x times slower
//                         // let child2Stats = fs.lstatSync(fullChild2Path)
//                         // if (child2Stats.isDirectory() && dirDefaultBlacklist.indexOf(path.basename(child2)) === -1) {
//                         //     childFolder.hasChildren = true
//                         //     break;
//                         // }
//                     }

//                     resultFolder.children.push(childFolder)
//                 } 
//             } catch (error) {
//                 console.log("scanDirForFolders => error "+ error);    
//             }
//         });
//     } 
//     return resultFolder
// }