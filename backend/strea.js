const express = require('express')
const conf = require('./config.js')
const uploadLib = require('./upload.js')
const listLib = require('./list.js')
const streamLib = require('./stream.js')
const streamStatsLib = require('./streamStats.js')
var fs = require('fs');

const app = express()
// const apptoken = '3219qr8y01f73687fhdip0aqfe217834x7r0uf1nd81e36wx673917r931h2c34d98132'
// app.use(express.static('public'));
app.use('/', express.static('client'));

uploadLib.initUploadLogic(app);


app.get('/api', (req, res) => { 
		const q = {
				token: req.query.token,
				action:  req.query.action,
				path:  req.query.path,
				args:  req.query.args
		}
		
    /**
     * LIST OF ACTIONS 
     */
    if (q.action && q.action === 'list' && q.path) {
				answer = `list function!`
				const pathFolder =  q.path
				
				listLib.getFolderHierarchySync(pathFolder)
						.then(files => {
								res.send(files)
						})
						.catch(error => {
								res.send(`ERROR ${JSON.stringify(error)}`)
						});
				
				/**
				 * STREAM
				 * http://localhost:4044/?args={quality:31}&action=stream&token=3219qr8y01f73687fhdip0aqfe217834x7r0uf1nd81e36wx673917r931h2c34d98132
				 * http://localhost:4044/?path=eeee&quality=22&audio=0&hour=00&min=10&sec=04&size=600&action=stream&token=3219qr8y01f73687fhdip0aqfe217834x7r0uf1nd81e36wx673917r931h2c34d98132
				 * http://localhost:4044/?action=stream&token=3219qr8y01f73687fhdip0aqfe217834x7r0uf1nd81e36wx673917r931h2c34d98132
				 */
		} else if (q.action && q.action === 'stream') {
				const p = {
						path: req.query.path || 'nopath',
						quality: req.query.quality || '32',
						audioquality: req.query.audioquality || '96',
						stereo: req.query.stereo || '1',
						speed: req.query.speed || '6',
						audio: req.query.audio || '0',
						volume: req.query.volume || '1.5',
						hour: req.query.hour || '01',
						min: req.query.min || '02',
						sec: req.query.sec || '03',
						size: req.query.size || '1200',
				} 
				console.log('API REQ STREAM: params ',p)
				let answer = streamLib.ffmpegCmd(p)
				res.send(answer)

		} else if (q.action && q.action === 'countfiles') {
				let countNb = 0
				fs.readdirSync(conf.params.pathStream).map(async (child) => {countNb++})
				res.send({count: countNb})
		} 

		/**
		 * STREAM STATS 
		 */
		else if (q.action && q.action === 'streamstats') {
				streamStatsLib.calculateThenAnswer(res)
		} else {
				answer = 'invalid action'
				res.send(answer)
		}
})

app.listen(conf.params.port, 'localhost', () => {
		console.log(`Example app listening at http://localhost:${conf.params.port}`)
})


/**
 * 
 * LIST
 * https://strea.websocial.cc:4044/api?path=files/torrent/_towatch/series/&action=list&token=3219qr8y01f73687fhdip0aqfe217834x7r0uf1nd81e36wx673917r931h2c34d98132
 * 
 * STREAM
 * https://strea.websocial.cc:4044/api?path=../files/torrent/_towatch/series//Altered%20Carbon/Altered.Carbon.S02.COMPLETE.1080p.NF.WEB-DL.DDP5.1.x264-NTG[TGx]/Altered.Carbon.S02E01.Phantom.Lady.1080p.NF.WEB-DL.DDP5.1.x264-NTG.mkv&quality=22&audio=0&hour=00&min=10&sec=04&size=600&action=stream&token=3219qr8y01f73687fhdip0aqfe217834x7r0uf1nd81e36wx673917r931h2c34d98132
 * 
 * 
 * https://open-public-343536.websocial.cc/stream/test.m3u8
 * 
 * https://hls-js.netlify.app/demo/?src=https%3A%2F%2Fopen-public-343536.websocial.cc%2Fstream%2Ftest.m3u8&demoConfig=eyJlbmFibGVTdHJlYW1pbmciOnRydWUsImF1dG9SZWNvdmVyRXJyb3IiOnRydWUsInN0b3BPblN0YWxsIjpmYWxzZSwiZHVtcGZNUDQiOmZhbHNlLCJsZXZlbENhcHBpbmciOi0xLCJsaW1pdE1ldHJpY3MiOi0xfQ==
 * 
 * 
 * 
 */


