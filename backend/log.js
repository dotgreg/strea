const conf = require('./config.js')
exports.txt = string => {
  require('fs').appendFileSync(conf.params.pathLog, string);
}
exports.hist = string => {
  require('fs').appendFileSync(conf.params.pathHistory, string + '\n');
}
