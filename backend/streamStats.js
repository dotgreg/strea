
exports.calculateThenAnswer = (res) => {
    // GET STATS OF FILE STREAM
    // https://strea.websocial.cc/api?action=streamstats
    var exec = require('child_process').exec;
		const conf = require('./config.js')
    function execute(command, callback){
        exec(command, function(error, stdout, stderr){ callback(stdout); });
    };
    const cmdSize = `du -sh ${conf.params.pathStream}`
    const cmdNb = `ls ${conf.params.pathStream} | wc -l`
    execute(cmdSize, (answerSize) => {
        execute(cmdNb, (answerNb) => {
            let arr = answerSize.split('M')
            let raw = arr[0]
            let resSizeInt = parseInt(raw)

            let resNbInt = parseInt(answerNb) -2 
            let minTot = Math.round((resNbInt * 10) / 60)

            const sizePFile = resSizeInt / resNbInt
            const sizePerSec = sizePFile / 10
            const sizePerSecInK = Math.round(sizePerSec * 1000)
            const sizePerHour = Math.round(sizePerSec * 60 * 60)

            const resStr =resNbInt > 4 ? `stats: ${sizePerHour}M/h ${sizePerSecInK}k/s (${resSizeInt}M ${minTot}min)`  : `stats: waiting for more data... (${resNbInt} files)`
            // console.log(resStr)
            res.send({stats: resStr})
        });
    });
}
