const pathApp =  '/home/sigis/streaming/strea';
const pathFolderToStream =  '/home/sigis/streaming/fichiers';
const pathStream =  '/home/sigis/streaming/cache_stream/';

exports.params = {
    pathStream,
    pathLog : `${pathStream}/test.txt`,
    pathStreamFiles : `${pathStream}/test*`,
		pathHistory:  `${pathStream}/history.txt`,
    // pathSubtitles : '/home/greg/open_public/subtitles',
    // pathSubtitles : '../../testSubs',
    port : 4044
}
