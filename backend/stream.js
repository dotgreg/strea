const Log = require('./log.js')
const cp = require("child_process");
const conf = require('./config.js');
exports.ffmpegCmd = (p) => {

  const speedArr = ['placebo','veryslow', 'slower', 'slow', 'medium', 'fast', 'faster', 'veryfast', 'superfast', 'ultrafast'] 

  const cmdArr = [
    // `ls -l`,
    `pkill ffmpeg;`,
		`rm -r ${conf.params.pathStreamFiles};`,
    `ffmpeg -v info `,
    `-ss ${p.hour}:${p.min}:${p.sec}`,  
    `-re -nostdin -i "${decodeURIComponent(p.path)}"`,
    // `-c:v libx264 `,
    // `-crf ${p.quality} -vf`,
    // `scale=-2:${p.size}`,
    `-c:v:1 libx264 -crf ${p.quality} -preset ${speedArr[p.speed]} -sc_threshold 0 -keyint_min 48`,
    `-filter:a "volume=${p.volume}"`,
    `-acodec libmp3lame ${p.stereo === '0' ? '-ac 1' : ''} -b:a ${p.audioquality}k  -preset:v ${speedArr[p.speed]} -map 0:v:0 -map 0:a:${p.audio}`,
    `-hls_time 10 -hls_list_size 0 -start_number 1`,
		`${conf.params.pathStream}/test.m3u8`,
  ]
  const cmdStr = cmdArr.join(' ')
  // ffmpeg -v info  -ss 0:0:0 -re -nostdin -i "../files/torrent/_towatch/series//nma/Introduction to Watercolor _ New Masters Academy.mp4" -c:v:1 libx264 -crf 35 -preset slower  -sc_threshold 0 -keyint_min 48 -filter:a "volume=3" -acodec libmp3lame -ac 1 -b:a 40k -preset:v slower -map 0:v:0 -map 0:a:0 -hls_time 10 -hls_list_size 0 -start_number 1 /home/ubuntu/open_public/stream/test.m3u8
  // EXEC
  console.log(`ffmpeg cmd : executing ==> ${cmdStr}`);
  var child = cp.exec(cmdStr);
  
  let first = true
  child.stderr.on('data', function (data) {
    if (first) {
      Log.txt(`=====================================\n\n`)
      Log.txt(`ffmpeg cmd : executing ==> ${cmdStr}\n`)
      Log.txt(`=====================================\n\n`)
      first = false
    }
    // LOG
    Log.txt(data.toString())
  });
	  Log.hist(p.path)
}
	//can i start typing that wayy. It seems more appropriate than what I am used to. However I am still making a lot of mistake as I never really corrected my way of writing. I should then go back to my basic again before pretending building on it especially vim that requires a perfect mastery of that way of typing. 

