const fs = require ('fs')
const path = require ('path')

exports.getFolderHierarchySync = async (file, root) => {
 return new Promise((resolve, reject) => {
     var stats = fs.lstatSync(file)
     let parent= {
         path: file,
         title: path.basename(file),
         type:'folder'
     };
     fs.readdirSync(file).map(async (child) => {
         let childFile = file + '/' + child
         try {
           if (!parent.children) parent.children = []
             let stats2 = fs.lstatSync(childFile)
             if (stats2.isDirectory()) {
                 parent.children.push(await exports.getFolderHierarchySync(childFile))
             } else {
               parent.children.push({
                 path: childFile,
                 title: path.basename(childFile),
                 type:'file'
               })
             }
         } catch (error) {
             console.log("getFolderHierarchySync => error "+ error);    
         }
     });
     console.log('[SCAN FOLDER]', file, parent)
     resolve(parent)
 })
}
